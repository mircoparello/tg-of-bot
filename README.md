![cfd](https://www.symscape.com/files/pictures/misc/elephant.png)

## tg-of-bot 

This is a Telegram bot that reads and sends residuals, last timestep, last deltaT
and last Courant number from the simulation log file.  
This bot does not mean to be complete in any way, because it was tailored for my
needs while I was writing my Master's thesis. For example, I only needed to plot
pressure residuals over time, so the command `/residuals` only plots that. While
plotting other quantities is trivial, you'll have to add it on your own.  
The same reasoning applies to other commands: the only ones provided are those
I needed. If you want to add more functionality, feel free to create your own fork
of the project.  
Due to my laziness, and because I'm not a programmer, there's a high chance that
the code provided sucks. I am aware of this possibility, and I apologize in advance.

---

## Instructions
1. Clone the bot repo: `git clone https://mircoparello@bitbucket.org/mircoparello/tg-of-simulation-checker.git`
2. Install requirements: `pip install -r requirements.txt`  
**NB: if you don't have admin rights use** `pip install --user -r requirements.txt`
3. Open Telegram and create a new bot with [BotFather](https://t.me/BotFather). Give it a name, and BotFather will give you your secret token and the link to reach
your bot.
4. **Optional**, but recommended: customize your bot by adding the commands list.
Type `/setcommands` to BotFather and follow the instructions. 
4. Open `scripts/tg_of_bot.py` and paste the secret token in the `secret_token` variable
5. Copy the `scripts` folder to your OpenFOAM case folder
6. Run the bot inside the `scripts` folder with `python3 tg_of_bot.py` or, if you
want the bot to keep running after you close your ssh connection, `nohup python3 tg_of_bot.py &`
7. Done! Now open your bot on Telegram, and interact with it using the commands defined

---

## Commands
`/residuals` - sends a logarithmic plot of the pressure residual trend    
`/time` - sends the last timestep   
`/deltaT` - sends the last time increment  
`/courant` - sends the last Courant number  

---

## Limitations
Paths for residuals are hard coded because I was too lazy to do otherwise. If you
check `residuals_bot.py`, you'll find that the bot searches data in `postProcessing/residuals/0/residuals.dat`. Of course, if your residuals are in a different location, you have to change the path.  
The same applies to the log name, which is hard coded to `log.pimpleFoam`. Depending from your case, you might want to change these values inside the `search_pattern` function in `tg_of_bot.py`.
