import os
import logging
import re
import telegram
from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)

secret_token = 'YOUR_TELEGRAM_TOKEN_HERE'

bot = telegram.Bot(token = secret_token)

# Define command handlers

def start(update: Update, _: CallbackContext) -> None:
    """Send a greeting when the command /start is issued."""
    user = update.effective_user
    update.message.reply_markdown_v2(
        fr'Hi {user.mention_markdown_v2()}\!'
    )

def search_pattern(word):
    """Given a word, searches the pattern inside the log"""
    file = open("../log.pimpleFoam", "r")
    lines = []
    for line in file:
        if re.search(word, line):
            lines.append(line)
    return lines[-1]

def check_folder(folder_name):
    """Checks if a folder exists. If not, it creates the folder named folder_name."""
    folders_list = set(os.listdir(".."))
    if folder_name in folders_list:
        pass
    else:
        os.mkdir("../" + folder_name)

def generate_residuals_plot():
    """Executes residuals_plot.py to get the residuals trend picture. This is a 
    workaround to matplotlib limitations.
    """
    os.system("python3 residuals_plot.py")

def print_timestep(update: Update, _: CallbackContext) -> None:
    """Search for last timestep"""
    last_timestep = search_pattern("Time =")
    update.message.reply_text(last_timestep)

def print_courant(update: Update, _: CallbackContext) -> None:
    """Search for last Courant"""
    last_timestep = search_pattern("Courant Number")
    update.message.reply_text(last_timestep)

def print_deltaT(update: Update, _: CallbackContext) -> None:
    """Search for last deltaT"""
    last_timestep = search_pattern("deltaT")
    update.message.reply_text(last_timestep)

def send_residuals(update: Update, _: CallbackContext) -> None:
    """Checks if plots folder exist, then generates residual history picture and 
    saves it to 'plots' folder
    """
    check_folder("plots")
    generate_residuals_plot()
    bot.send_photo(chat_id = update.effective_chat.id, photo=open('../plots/residuals.png', 'rb'))


def main() -> None:
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(secret_token, use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Adds commands available from Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("time", print_timestep))
    dispatcher.add_handler(CommandHandler("courant", print_courant))
    dispatcher.add_handler(CommandHandler("timestep", print_deltaT))
    dispatcher.add_handler(CommandHandler("residuals", send_residuals))

    # Starts the bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT.
    updater.idle()


if __name__ == '__main__':
    main()