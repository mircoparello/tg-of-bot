import numpy as np
import matplotlib
import matplotlib.pyplot as plt

residuals_table = np.genfromtxt('../postProcessing/residuals/0/residuals.dat', skip_header=1)

t, p = [], []

for row in residuals_table:
    t.append(row[0])
    p.append(row[1])

fig, ax = plt.subplots()
ax.grid()
ax.set_title('Initial residuals', fontweight = "bold")
ax.plot(t, p, color = 'red')
ax.set_xlabel('Time')
ax.set_yscale('log')
ax.legend('p')
fig.savefig('../plots/residuals.png', dpi = 250)